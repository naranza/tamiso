<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_family_delete(string $family_name): void
{
  unset($_SESSION['tamiso']['family'][$family_name]);
}
