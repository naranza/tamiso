<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_family_get(array &$family, string $parent)
{
  if ('' !== $caller_url) {
    if (isset($_SESSION[$parent]['caller'])) {
      array_unshift($family, $_SESSION[$caller_url]['caller']);
    }
    tamiso_family_get($family, $_SESSION[$caller_url]['caller']['url'] ?? '');
  }
}
