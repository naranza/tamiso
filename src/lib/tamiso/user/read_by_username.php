<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/pgsql/prex.php';

function tamiso_user_read_by_username(pgsql\connection $conn, string $username): array|false
{
  $sql = "select
    user_id as id,
    user_username as username,
    user_password as password,
    user_status_id as status_id,
    user_fullname as fullname
    from tamiso_user
    where user_username = $1
    limit 1";

  $result = sesto_pgsql_prex($conn, __FUNCTION__, $sql, [$username]);
  $user = false;
  if ($result instanceof pgsql\result) {
    $user = pg_fetch_assoc($result);
    if (false !== $user) {
      $user['id'] = (int) $user['id'];
      $user['status_id'] = (int) $user['id'];
    }
  }
  return $user;
}
