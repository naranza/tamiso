<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_default_views(array $system): array
{
  return array_merge(
    $system['config']['view'] ?? [],
    $system['page']['view'] ?? [],
    [
      'layout' => TAMISO_VIEW_DIR . '/layout_default.phtml',
      'main_menu' => TAMISO_VIEW_DIR . '/main_menu.phtml',
      'page_header' => TAMISO_VIEW_DIR . '/page_header.phtml',
      'page_main' => TAMISO_VIEW_DIR . '/page_main.phtml',
      'page_footer' => TAMISO_VIEW_DIR . '/page_footer.phtml',
      'alert' => TAMISO_VIEW_DIR . '/alert.phtml'
    ]
  );
}
