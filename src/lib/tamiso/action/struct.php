<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_action_struct(): array
{
  return [
    'name' => '',
    'caption' => '',
    'screen' => ''
  ];
}
