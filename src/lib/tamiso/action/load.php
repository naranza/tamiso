<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/action/read.php';
require_once TAMISO_DIR . '/action/compile.php';

function tamiso_action_load(array $app, string $object_dir): array
{
  return tamiso_action_compile($app, tamiso_action_read($object_dir));
}
