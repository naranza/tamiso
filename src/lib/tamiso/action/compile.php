<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/action/struct.php';

function tamiso_action_compile(array $app, array $object): array
{
//  $compiled = [];
  $compiled = array_merge(tamiso_action_struct(), $object);
  $compiled['caption'] = $object['caption'] ?? '';
  $compiled['screen'] = $object['screen'] ?? '';

  return $compiled;
  sesto_d($compiled);
  die;
  $compiled['family_name'] = $object['family_name'] ?? '';
  $compiled['family_level'] = $object['family_level'] ?? -1;
  $compiled['family_parent_level'] = $compiled['family_level'] - 1;
  $compiled['family_caption_field'] = $compiled['family_caption_field'] ?? '';
}
