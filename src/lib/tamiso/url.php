<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

function tamiso_url(array $route, string $destination = ',,,', array $params = []): string
{
  $out = '';
  if (false !== strpos($destination, ',')) {
    $parts = explode(',', substr($destination, 0));
    $out = '/';
    $out .= (isset($parts[0]) && '' !== $parts[0]) ? $parts[0] : $route['module'];
    $out .= '/';
    $out .= (isset($parts[1]) && '' !== $parts[1]) ? $parts[1] : $route['object'];
    $out .= '/';
    $out .= (isset($parts[2]) && '' !== $parts[2]) ? $parts[2] : $route['action'];
  } else {
    $out .= '/' . ltrim($destination, '/');
  }
  /* prepend url_base */
  if ('/' !== $route['url_base'] ?? '/') {
    $out = $route['url_base'] . $out;
  }
  /* add params */
  if (!empty($params)) {
    $out .= '?' . http_build_query($params);
  }
  return $out;
}

