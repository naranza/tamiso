<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/core/config_read.php';

function tamiso_compile_buttons(array $app, array $actions): array
{
  $buttons = ['header' => [], 'footer' => []];
  foreach ($actions as $name => $action) {
//    $position = 'footer';
//    switch (strtolower(trim($name))) {
//      case 'create':
//        $action['data_context'] = false;
//        $content = '<i class="fas fa-plus-circle"></i> Create';
//        $class_suffix = ' t-btn-goto';
//        $target_route = ',,create';
//        $params['caller'] = $app['route']['url_relative'];
//        break;
//      case 'update':
//        $action['data_context'] = true;
//        $content = '<i class="fas fa-pen"></i> Update';
//        $target_route = ',,update';
//        break;
//      case 'delete':
//        $content = $action['content'] ?? 'Delete';
//        $target_route = ',,delete';
//        break;
//      case 'filter':
//        $action['data_context'] = false;
//        $content = '<span class="fas fa-filter"></span> Filter';
//        $class_suffix = ' t-btn-goto';
//        $target_route = 'tamiso,filter,set';
////        $params['caller'] = $app['caller'];
//        break;
//      case 'read':
//        $action['data_context'] = false;
//        $content = 'Read';
//        $target_route = ',,read';
//        break;
//      default:
//        $action['data_context'] = (bool) ($action['data_context'] ?? true);
//        $content = $action['content'] ?? $name;
//        break;
//    }
    $params = [];
    $data = 'data' === $action['type'];
    $class_suffix = 'goto' === $action['type'] ? ' t-btn-goto' : '';
    $buttons[($data ? 'footer' : 'header')][] = [
      'tag' => 'button',
      'attribs' => [
        'id' => 't-btn-action-' . $action['name'],
        'type' => ($data ? 'submit' : 'button'),
        'name' => 't-action',
        'data-target' => tamiso_url($app['route'], $action['target_route'], $params),
        'class' => 'toco-btn toco-btn-primary' . $class_suffix,
        'value' => tamiso_url($app['route'], $action['target_route'], $params),
        'value' => $name,
        'form' => 't-context-form'
      ],
      'content' => $action['caption']
    ];
  }
//  sesto_d($buttons);
//  die;
  return $buttons;
}
