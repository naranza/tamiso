<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/html/escape.php';

function tamiso_screen_escape(array $screen, array $record): array
{
  /* */
  $escaped = $screen;
  foreach ($screen['elements'] as $field_name => $element) {
    $value = $record[$field_name] ?? null;
    if (null !== $value) {
      switch ($element['tag']) {
        case 'input':
          $escaped[$field_name]['attribs']['value'] = tamiso_html_escape($value);
          break;
        case 'textarea':
          $escaped[$field_name]['attribs']['content'] = tamiso_html_escape($value);
          break;
      }
    }
  }
  return $escaped;
}
