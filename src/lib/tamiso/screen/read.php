<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_screen_read(string $path): array
{
  $path .= 'php' != pathinfo($path, PATHINFO_EXTENSION) ? '.php' : '';
  $result = [];
  if (is_file($path) && is_readable($path)) {
    $result = include($path);
    if (isset($config) && is_array($config)) {
      $result = $config;
    } elseif (!is_array($result)) {
      $result = [];
    }
    $type = $result['type'] ?? '';
    switch ($type) {
      case 'form':
        $result['elements'] ?? $result['elements'] = [];
        $result['buttons'] ?? $result['buttons'] = [];
        $result['cells'] ?? $result['cells'] = [];
        foreach ($result['cells'] as $key => $cell) {
          $result['cells'][$key]['label_visible'] = $cell['label_visible'] ?? true;
          $result['cells'][$key]['label_attribs'] = $cell['label_attribs'] ?? [];
        }
        $result['rows'] ?? $result['rows'] = [];
        $result['fieldsets'] ?? $result['fieldsets'] = [];
        $result['form'] ?? $result['form'] = [];
        $result['hiddens'] ?? $result['hiddens'] = [];
        break;
      case 'list':
        $result['id'] ?? $result['id'] = [];
        $result['cols'] ?? $result['cols'] = [];
        break;
      default:
        $result = [];
    }
  }
  return $result;
}
