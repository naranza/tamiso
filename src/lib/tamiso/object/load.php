<?php

/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/object/read.php';
require_once TAMISO_DIR . '/object/compile.php';

function tamiso_object_load(array $app, string $object_dir): array
{
  return tamiso_object_compile($app, tamiso_object_read($object_dir));
}
