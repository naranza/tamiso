<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once SESTO_DIR . '/core/config_read.php';

function tamiso_read_actions(string $dir): array
{
  return sesto_config_read($dir . '/actions');
}