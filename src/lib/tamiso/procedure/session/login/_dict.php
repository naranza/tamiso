<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

$config = [
  'username' => [
    'rule' => ['tamiso::rule/not_empty_string']
  ],
  'password' => [
    'rule' => ['tamiso::rule/not_empty_string']
  ],
];

