<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

return [
  'type' => 'form',
  'elements' => [
    'submit' => [
      'tag' => 'button',
      'attribs' => [
          'type' => 'submit',
          'value' => 'login'
        ],
      'content' => 'Login'
    ],
    'username' => [
      'tag' => 'input',
      'attribs' => ['type' => 'text']
    ],
    'password' => [
      'tag' => 'input',
      'attribs' => ['type' => 'password']
    ]
  ],
  'buttons' => ['submit'],
  'cells' => [
    0 => ['label' => 'Username', 'elements' => ['username']],
    1 => ['label' => 'Password', 'elements' => ['password']]
  ],
  'rows' => [
    0 => ['cells' => [0, 1]]
  ],
  'fieldsets' => [
    0 => [
      'visible' => false,
      'attribs' => ['class' => '123'],
      'legend' => 'this is my lengend',
      'legend_visible' => true,
      'legend_attribs' => ['class' => 'my-legend'],
      'rows' => [0],
    ]
  ],
  'form' => [
    'fieldsets' => [0],
  ],
];

