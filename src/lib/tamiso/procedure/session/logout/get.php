<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

class tamiso_procedure
{

  public $auth_check = false;

  public function run(array $app)
  {
    $_SESSION = [];
    session_destroy();

    sesto_http_redirect(tamiso_url($app['route'], 'tamiso,session,login'));
  }

}