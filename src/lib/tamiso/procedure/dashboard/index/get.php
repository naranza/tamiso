<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/default/viewdata.php';
require_once TAMISO_DIR . '/default/views.php';
require_once SESTO_DIR . '/view/render.php';

class tamiso_procedure
{

  public bool $check_auth = true;

  public function run(array $app)
  {
    $session_mo = &$app['route']['dirname'];

    /* view data */
    $viewdata = tamiso_default_viewdata($app);
    $viewdata['alert'] = tamiso_alert_read($session_mo);

    /* views */
    $views = tamiso_default_views($app);
    $views['page_content'] = __DIR__ . '/view.phtml';

    /* render */
    sesto_view_render($views, 'layout', $viewdata);
  }

}