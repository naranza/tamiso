<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

require_once TAMISO_DIR . '/bcrumb/read.php';

function tamiso_bcrumb_build(string $index, &$d): void
{
  if (false === $_SESSION[$index]['sess_parent']) {
    array_unshift($d, tamiso_bcrumb_read($index));
  } else {
    array_unshift($d, tamiso_bcrumb_read($index));
    tamiso_bcrumb_build($_SESSION[$index]['sess_parent'], $d);
  }
}