<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_bcrumb_read(string $namespace): ?array
{
  return $_SESSION[$namespace]['bcrumb'] ?? null;
}
