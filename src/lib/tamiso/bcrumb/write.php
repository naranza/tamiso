<?php
/* =============================================================================
 * Naranza Tamiso, Copyright (c) Andrea Davanzo, License MPL v2.0, naranza.org
 * ========================================================================== */

declare(strict_types=1);

function tamiso_bcrumb_write(string $namespace, string $label, string $url): void
{
  $_SESSION[$namespace]['bcrumb'] = ['label' => $label, 'url' => $url];
}
